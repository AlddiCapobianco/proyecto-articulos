let main = document.querySelector("#contenido");
fetch("rubros.csv")
    .then(function (res) {
        return (res.text());
    })
    .then(function (data) {
        cargaRubros(data);
    })

fetch("listas.csv")
    .then(function (res) {
        return (res.text());
    })
    .then(function (data) {
        mostrarTabla(data);
    })

const rubros = [];
function cargaRubros(data_rubros) {
    let filas_rubros = data_rubros.split(/\r?\n|\r/);
    for (let i = 0; i < filas_rubros.length; i++) {
        let datos_rubros = filas_rubros[i].split(',');
        rubros[i] = { codigo: datos_rubros[0], descripcion: datos_rubros[1], fondo: datos_rubros[2], color: datos_rubros[3] }
        //console.log(rubros[i].codigo," - ",rubros[i].descripcion,rubros[i].fondo,rubros[i].color);
    }

}
//console.log(rubros);

//console.log(rubros.length);

function mostrarTabla(contenido) {
    let template = ``;
    for (let r = 0; r < rubros.length; r++) {
        let descripcion = removeAccents(rubros[r].descripcion).toLowerCase();
        let descripMayus = rubros[r].descripcion.toUpperCase();

        template += `<section class="parallax" id="${descripcion}">`;
        template += `<h3>${descripMayus}</h3>`;
        template += `<div class="contenedor">`;

        template += armarTemplateArticulos(r, contenido);

        template += `</div>`;
        template += `</section>`;
    }
    main.innerHTML = template;

    for (let r = 0; r < rubros.length; r++) {
        let descripcion = removeAccents(rubros[r].descripcion).toLowerCase();
        let seccion = document.querySelector(`#${descripcion}`);
        seccion.style.backgroundImage = `linear-gradient(to bottom, rgba(255,255,255,0.3),rgba(255,255,255,0.3)), url(${rubros[r].fondo})`;
        seccion.style.color = `${rubros[r].color}`;

    }
}

function armarTemplateArticulos(r, contenido) {
    let filas = contenido.split(/\r?\n|\r/);
    let subTemplate = ``;
    for (let i = 0; i < filas.length; i++) {
        let celdasFila = filas[i].split(',');
        if (celdasFila[0] === (r + 1).toString()) {
            subTemplate += `<article>`;
            subTemplate += `<img src="${celdasFila[1]}" alt=""/>`
            subTemplate += `<h4>${celdasFila[2]}</h4>`
            subTemplate += `</article>`;
        }
    }
    return subTemplate;
}

const removeAccents = (str) => {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
};